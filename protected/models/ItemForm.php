<?php
class ItemForm extends CFormModel
{
    public $id;
    public $id_category;
    public $id_trenk;
    public $title;
    public $description;
    public $id_image;



    public function rules()
    {
        return array(
            array('id_category, title, description, update_time, id_image','required'),
            array('id_category,id_image', 'numerical', 'integerOnly'=>true),
            array('title', 'length', 'max'=>100),
            array('update_time', 'safe'),
            array('id, id_user, title, date_create, date_update', 'safe', 'on'=>'search'),
        );

    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'id_category' => 'Id Каталога',
            'id_trenk' => 'Тренк',
            'title' => 'Заголовок',
            'description' => 'Описание',
            'id_image' => 'ID изображения',
        );
    }



}
