<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class SingupForm extends CFormModel
{
	public $username;
	public $email;
	public $paypalemail;
	public $password;

	//private $_identity;

	public function rules()
	{
		return array(
			array('username, email, password', 'required'),
			array('gender', 'numerical', 'integerOnly' => true),
			array('firstname, lastname', 'length', 'max' => 25),
			array('email', 'length', 'max' => 50),
			array('email,paypalemail', 'email'),
			array('password', 'length', 'max' => 32),
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'firstname' => 'Имя',
			'lastname' => 'Фамилия',
			'email' => 'Email',
			'paypalemail' => 'paypalemail',
			'password' => 'Пароль',
		);
	}

	public function authenticate($attribute, $params)
	{
		if (!$this->hasErrors()) {
			$this->_identity = new UserIdentity($this->username, $this->password);
			if (!$this->_identity->authenticate()) {
				$this->addError('password', 'Incorrect username or password.');
			}
		}
	}

	public function login()
	{
		if ($this->_identity === null) {
			$this->_identity = new UserIdentity($this->username, $this->password);
			$this->_identity->authenticate();
		}
		if ($this->_identity->errorCode === UserIdentity::ERROR_NONE) {
			$duration = $this->rememberMe ? 3600 * 24 * 30 : 0; // 30 days
			Yii::app()->user->login($this->_identity, $duration);
			return true;
		} else {
			return false;
		}
	}
}
