<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class Userinfo extends CFormModel
{
    public $user_id;
    public $firstname;
    public $lastname;
    public $gender;
    public $group;
    public $user_pref;
    public $user_status;
    public $latitude;
    public $longitude;
    public $avatar;
    public $distance;
    public $email;
 
    

    

    /**
     * Declares the validation rules.
     * The rules state that username and password are required,
     * and password needs to be authenticated.
     */
    public function rules()
    {
        return array(
            // username and password are required
            /*array('user_id', 'required'),
            array('user_id,gender,group,user_pref', 'numerical', 'integerOnly'=>true),
            */
            
        );
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels()
    {
        return array(
            //'rememberMe'=>'Remember me next time',
        );
    }

}