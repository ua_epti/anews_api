<?php

class Tasks extends CFormModel
{
	public $userID;
	public $taskID;
	public $content;
	public $video;
	public $image;

	public function rules()
	{
		return array(
			array('userID,taskID', 'required'),
			array('userID,taskID', 'required', 'on' => 'complete'),
			array('image', 'file', 'types' => 'jpg, gif, png'),
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array();
	}

}