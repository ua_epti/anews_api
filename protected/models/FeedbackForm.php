<?php

/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class FeedbackForm extends CFormModel
{


	public $id;
	public $first_name;
	public $last_name;
	public $email;
	public $category;
	public $importance;
	public $status;
	public $get_date;
	public $question;
	public $answer;

	public function rules()
	{
		if (!Yii::app()->user->isGuest) {
			return array(
				array('category, question', 'required'),
				array('email', 'email'),
				array('category, importance, status', 'numerical', 'integerOnly' => true),
				array('first_name, last_name', 'length', 'max' => 50),
				array('id, first_name, last_name, email, category, importance, status, get_date, question, answer', 'safe', 'on' => 'search'),
			);
		} else {
			return array(
				array('first_name, last_name, email, category, question', 'required'),
				array('email', 'email'),
				array('category, importance, status', 'numerical', 'integerOnly' => true),
				array('first_name, last_name', 'length', 'max' => 50),
				array('id, first_name, last_name, email, category, importance, status, get_date, question, answer', 'safe', 'on' => 'search'),

			);
		}


	}

	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'first_name' => 'Имя',
			'last_name' => 'Фамилия',
			'email' => 'e-mail',
			'category' => 'Категория',
			'importance' => 'Уровень важности',
			'status' => 'Статус',
			'get_date' => 'Дата отправки',
			'question' => 'Вопрос',


		);
	}
}