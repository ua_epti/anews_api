<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class Comment extends CFormModel
{
    public $item_id;
    public function rules()
    {
        return array(
            // username and password are required
            array('item_id', 'required'),
            array('item_id', 'numerical'),
            
        );
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels()
    {
        return array(
            'rememberMe'=>'Remember me next time',
        );
    }

}