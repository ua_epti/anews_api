<?php
class TrenkForm extends CFormModel
{
    public $id;    
    public $id_user;
    public $title;
    public $date_create;
    public $date_update;

   

    /**
     * Declares the validation rules.
     * The rules state that username and password are required,
     * and password needs to be authenticated.
     */
    public function rules()
    {
        return array(
            array('id_user, title, date_create', 'required'),
            array('id_user', 'numerical', 'integerOnly'=>true),
            array('title', 'length', 'max'=>100),
            array('date_update', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, id_user, title, date_create, date_update', 'safe', 'on'=>'search'),
        );
        
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'id_user' => 'Id пользователя',
            'title' => 'Заголовок',
            'date_create' => 'Дата создания',
            'date_update' => 'Дата обновления',
        );
    }


  
}
