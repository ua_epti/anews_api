<?php
class News extends CFormModel
{
	public $id;
	public $userID;
	public $is_avaliable;

	public function rules()
	{
		return array(
			array('userID', 'required'),
			array('userID,timeCreate', 'numerical', 'integerOnly' => true),
		);
	}
}