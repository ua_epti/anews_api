<?php echo "controller = " . yii()->controller->id . " ";
echo "</br>";
echo "action = " . yii()->controller->action->id . " ";
echo "</br>";
echo "c = " . yii()->controller->id . " ";
echo "</br>";
echo "task = registration";
echo "</br>";
echo "</br>";
?>


<?=
CHtml::ajaxButton(
	'test_button',
	createUrl(yii()->controller->id . "/" . "singup"),
	array(
		'type' => "POST",
		'data' => array(
			'User' => array(
				'c' => yii()->controller->id,
				'task' => 'registration',
				'email' => 'js:($("#SingupForm_email").val())',
				'password' => 'js:($("#SingupForm_password").val())',
				'firstname' => 'js:($("#SingupForm_firstname").val())',
				'lastname' => 'js:($("#SingupForm_lastname").val())',
				'gender' => 'js:parseInt($("#SingupForm_gender").val())',
//				'latitude' => 'js:($("#SingupForm_latitude").val())',
//				'longitude' => 'js:($("#SingupForm_longitude").val())',
			),
		),
		'success' => "function(data){
    $('.form').append(data);

   }"
	)
)?>


<div class="form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'User',
		'enableClientValidation' => true,
		'clientOptions' => array(
			'validateOnSubmit' => true,
		),
	)); ?>



	<div class="row">
		<?php echo $form->labelEx($model, 'email'); ?>
		<?php echo $form->textField($model, 'email'); ?>
		<?php echo $form->error($model, 'email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model, 'password'); ?>
		<?php echo $form->passwordField($model, 'password'); ?>
		<?php echo $form->error($model, 'password'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model, 'username'); ?>
		<?php echo $form->textField($model, 'username'); ?>
		<?php echo $form->error($model, 'username'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model, 'paypalemail'); ?>
		<?php echo $form->textField($model, 'paypalemail'); ?>
		<?php echo $form->error($model, 'paypalemail'); ?>
	</div>
	<!--<div class="row">
        <?php /*echo $form->labelEx($model,'gender'); */?>
        <?php /*echo CHtml::dropDownList('SingupForm[gender]', $model->gender,
              array('1' => 'Мужской', '2' => 'Женский'));
        */?>
    </div>-->

	<div class="row buttons">
		<?php echo CHtml::submitButton('SingUp'); ?>
	</div>

	<?php $this->endWidget(); ?>
</div><!-- form -->