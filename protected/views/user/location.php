<?php echo "controller = " . yii()->controller->id . " ";
echo "</br>";
echo "action = " . yii()->controller->action->id . " ";
echo "</br>";
echo "c = " . yii()->controller->id . " ";
echo "</br>";
echo "task = ". yii()->controller->action->id . " ";
echo "</br>";echo "</br>";
?>


GEO поиск

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'login-form',
    'enableClientValidation'=>true,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    ),
)); ?>

    Мои координаты
    <div class="row">
        <?php echo $form->labelEx($model,'latitude'); ?>
        <?php echo $form->textField($model,'latitude',array('value'=>'48.732138')); ?>
        
        <?php echo $form->labelEx($model,'longitude'); ?>
        <?php echo $form->textField($model,'longitude',array('value'=>'37.583351')); ?>
    
    </div>
    Дистанция в метрах
    <div class="row">
        <?php echo $form->labelEx($model,'distance'); ?>
        <?php echo $form->textField($model,'distance',array('value'=>'500'));?>
        <?php echo $form->error($model,'distance'); ?>
    </div>
    <div class="row buttons">
        <?php echo CHtml::submitButton('Послать'); ?>
    </div>

<?php $this->endWidget(); ?>
</div><!-- form -->
