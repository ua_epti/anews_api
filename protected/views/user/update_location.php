<?php echo "controller = " . yii()->controller->id . " ";
echo "</br>";
echo "action = " . yii()->controller->action->id . " ";
echo "</br>";
echo "c = " . yii()->controller->id . " ";
echo "</br>";
echo "task = update_location";
echo "</br>";echo "</br>";
?>

Обновить местоположение<br />
iphone.php?c=user&task=update_location<br />

request data :<br />
{
    "latitude" : latitude // double<br />
    "longitude" : latitude // double<br />
}


<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'login-form',
    'enableClientValidation'=>true,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    ),
)); ?>


    <div class="row">
        <?php echo $form->labelEx($model,'user_id'); ?>
        <?php echo $form->textField($model,'user_id'); ?>
        <?php echo $form->error($model,'user_id'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'latitude'); ?>
        <?php echo $form->textField($model,'latitude',array('value'=>'51.526613503445766')); ?>
        <?php echo $form->error($model,'latitude'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'longitude'); ?>
        <?php echo $form->textField($model,'longitude',array('value'=>'46.02093849218558')); ?>
        <?php echo $form->error($model,'longitude'); ?>
    </div>
    <div class="row buttons">
        <?php echo CHtml::submitButton('Послать'); ?>
    </div>

<?php $this->endWidget(); ?>
</div><!-- form -->
