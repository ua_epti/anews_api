<?php echo "controller = " . yii()->controller->id . " ";
echo "</br>";
echo "action = " . yii()->controller->action->id . " ";
echo "</br>";
echo "c = " . yii()->controller->id . " ";
echo "</br>";
echo "task = upload_avatar";
echo "</br>";
echo "</br>";
?>



<?=
CHtml::ajaxButton(
	'test_button',
	createUrl(yii()->controller->id . "/" . "uploadavatar"),
	array(
		'type' => "POST",
		'data' => array(
			'Userinfo' => array(
				'c' => yii()->controller->id,
				'task' => 'upload_avatar',
				'avatar' => 'js:($("#Userinfo_avatar").val())',
				'user_id' => 'js:parseInt($("#Userinfo_user_id").val())',
			),
		),
		'success' => "function(data){
    $('.form').append(data);
   }"
	),
	array(
		'id' => 'ajaxButton',
	)
)?>

<div class="form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'login-form',
		'enableClientValidation' => true,
		'htmlOptions' => array('enctype' => 'multipart/form-data'),
		'clientOptions' => array(
			'validateOnSubmit' => true,
		),

	)); ?>


	<div class="row">
		<?php echo $form->labelEx($model, 'user_id'); ?>
		<?php echo $form->textField($model, 'user_id'); ?>
		<?php echo $form->error($model, 'user_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model, 'avatar'); ?>
		<?php echo $form->textField($model, 'avatar'); ?>
		<?php echo $form->error($model, 'avatar'); ?>
	</div>

	<div class="row">
		<? //php echo $form->labelEx($model,'avatar'); ?>
		<? //php echo $form->fileField($model,'avatar'); ?>
		<? //php echo $form->error($model,'avatar'); ?>
	</div>
	<div class="row buttons">
		<?php echo CHtml::submitButton('Послать'); ?>
	</div>

	<?php $this->endWidget(); ?>
</div><!-- form -->
