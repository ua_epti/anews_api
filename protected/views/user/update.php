<?php echo "controller = " . yii()->controller->id . " ";
echo "</br>";
echo "action = " . yii()->controller->action->id . " ";
echo "</br>";
echo "c = " . yii()->controller->id . " ";
echo "</br>";
echo "task = update_info";
echo "</br>";echo "</br>";
?>


Обновить свое инфо пользователя:<br />
iphone.php?c=user&task=update_info<br />
<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'login-form',
    'enableClientValidation'=>true,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    ),
)); ?>


    <div class="row">
        <?php echo $form->labelEx($model,'user_id'); ?>
        <?php echo $form->textField($model,'user_id'); ?>
        <?php echo $form->error($model,'user_id'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'firstname'); ?>
        <?php echo $form->textField($model,'firstname'); ?>
        <?php echo $form->error($model,'firstname'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'lastname'); ?>
        <?php echo $form->textField($model,'lastname'); ?>
        <?php echo $form->error($model,'lastname'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'user_status'); ?>
        <?php echo $form->textField($model,'user_status'); ?>
        <?php echo $form->error($model,'user_status'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'gender'); ?>
        <?php echo CHtml::dropDownList('Userinfo[gender]', $model->gender,
              array('1' => 'Мужской', '2' => 'Женский'));
        ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'group'); ?>
        <?php echo $form->textField($model,'group'); ?>
        <?php echo $form->error($model,'group'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'user_pref'); ?>
        <?php echo $form->textField($model,'user_pref'); ?>
        <?php echo $form->error($model,'user_pref'); ?>
    </div>
    
    <div class="row buttons">
        <?php echo CHtml::submitButton('Послать'); ?>
    </div>

<?php $this->endWidget(); ?>
</div><!-- form -->
