<?php echo "controller = " . yii()->controller->id . " ";
echo "</br>";
echo "action = " . yii()->controller->action->id . " ";
echo "</br>";
echo "c = " . yii()->controller->id . " ";
echo "</br>";
echo "task = messages";
echo "</br>";echo "</br>";
?>

Список сообщений<br />
iphone.php?c=chat&task=messages<br />
request data :<br />
{<br />
    “from_date” : date // timestamp<br />
}<br />
// OK<br />
response:<br />
{<br />
    "Result" : “success”<br />
    "Data" : [<br />
    {
        “message_id” : server_id<br />
        “sender_id” : user_id<br />
        “user_id” : user_id<br />
        “message” : message // string<br />
        “date” : date // timestamp - серверное время сообщения<br />
    }<br />
    ]<br />
}<br />
// ERROR<br />
response:<br />
{<br />
    "Result" : “error”<br />
    "Message" : message // string.<br />
}<br />

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'items-form',
    'enableAjaxValidation'=>false,
)); ?>
    <?php echo $form->errorSummary($model); ?>
    <div class="row">
        <?php echo $form->labelEx($model,'from_date');?>
        <?php echo $form->textField($model,'from_date',array('size'=>50,'maxlength'=>50,'value'=>'2012-09-05'));?>
        <?php echo $form->error($model,'from_date');?>
    </div>
    <div class="row buttons">
        <?php echo CHtml::submitButton('Запустить'); ?>
    </div>
<?php $this->endWidget(); ?>
</div>
