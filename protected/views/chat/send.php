<?php echo "controller = " . yii()->controller->id . " ";
echo "</br>";
echo "action = " . yii()->controller->action->id . " ";
echo "</br>";
echo "c = " . yii()->controller->id . " ";
echo "</br>";
echo "task = Login_Form";
echo "</br>";echo "</br>";
?>


Отправить сообщение чата<br />
iphone.php?c=chat&task=send_message<br />
request data :<br />
{<br />
    “user_id” : user_id // int<br />
    “message” : message // string<br />
}<br />

// OK<br />
response:<br />
{
    "Result" : “success”<br />
    "Data" : {<br />
        “message_id”: id // id сообщения<br />
        “date” : date // timestamp - серверное время сообщения<br />
    }<br />
}<br />

// ERROR<br />
response:<br />
{<br />
    "Result" : “error”<br />
    "Message" : message // string.<br />
}
<br />
<?php echo "controller = " . yii()->controller->id . " ";
echo "</br>";
echo "action = " . yii()->controller->action->id . " ";
echo "</br>";
echo "c = " . yii()->controller->id . " ";
echo "</br>";
echo "task = Login_Form";
echo "</br>";
?>



<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'items-form',
    'enableAjaxValidation'=>false,
)); ?>
    <?php echo $form->errorSummary($model); ?>
	<div class="row">
		<?php echo $form->labelEx($modelUser,'username'); ?>
		<?php echo $form->textField($modelUser,'username'); ?>
		<?php echo $form->error($modelUser,'username'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($modelUser,'password'); ?>
		<?php echo $form->passwordField($modelUser,'password'); ?>
		<?php echo $form->error($modelUser,'password'); ?>
	</div>
    <div class="row">
        <?php// echo $form->labelEx($model,'author_id'); ?>
        <?php echo $form->hiddenField($model,'author_id',array('value'=>'18')) ?>
        <?php echo $form->error($model,'author_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'getter_id'); ?>
        <?php echo $form->textField($model,'getter_id',array('value'=>'8')) ?>
        <?php echo $form->error($model,'getter_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'message'); ?>
        <?php echo $form->textArea($model,'message') ?>
        <?php echo $form->error($model,'message'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'latitude'); ?>
        <?php echo $form->textField($model,'latitude',array('value'=>'51.5266')); ?>
        <?php echo $form->error($model,'latitude'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'longitude'); ?>
        <?php echo $form->textField($model,'longitude',array('value'=>'46.0209')); ?>
        <?php echo $form->error($model,'longitude'); ?>
    </div>

	<input type="hidden" value="<?=$action?>" name="action">

    <div class="row buttons">
        <?php echo CHtml::submitButton('Отправить'); ?>
    </div>
    
<?php $this->endWidget(); ?>
</div>