<?php echo "controller = " . yii()->controller->id . " ";
echo "</br>";
echo "action = " . yii()->controller->action->id . " ";
echo "</br>";
echo "c = " . yii()->controller->id . " ";
echo "</br>";
echo "task = messages_read";
echo "</br>";echo "</br>";
?>

Прочитать сообщение<br />
iphone.php?c=chat&task=messages_read<br />
request data :<br />
{<br />
    “from_date” : date // timestamp<br />
}<br />
// OK<br />
response:<br />
{<br />
    "Result" : “success”<br />
    "Data" : [<br />
    {
        “message_id” : server_id<br />
        “sender_id” : user_id<br />
        “user_id” : user_id<br />
        “message” : message // string<br />
        “date” : date // timestamp - серверное время сообщения<br />
    }<br />
    ]<br />
}<br />
// ERROR<br />
response:<br />
{<br />
    "Result" : “error”<br />
    "Message" : message // string.<br />
}<br />

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'items-form',
    'enableAjaxValidation'=>false,
)); ?>
    <?php echo $form->errorSummary($model); ?>
    <div class="row">
        <?php echo $form->labelEx($model,'message_id');?>
        <?php echo $form->textField($model,'message_id',array('size'=>50,'maxlength'=>50,'value'=>''));?>
        <?php echo $form->error($model,'message_id');?>
    </div>
    <div class="row buttons">
        <?php echo CHtml::submitButton('Запустить'); ?>
    </div>
<?php $this->endWidget(); ?>
</div>
