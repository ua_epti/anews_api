<?php echo "controller = " . yii()->controller->id . " ";
echo "</br>";
echo "action = " . yii()->controller->action->id . " ";
echo "</br>";
echo "c = " . yii()->controller->id . " ";
echo "</br>";
echo "task = all";
echo "</br>";
echo "</br>";
?>
<div class="form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'tasks-form',
		'enableAjaxValidation' => false,
	)); ?>
	<?php echo $form->errorSummary($model); ?>
	<div class="row buttons">
		<?php echo CHtml::submitButton('Получить опубликованные задания'); ?>
	</div>
	<?php $this->endWidget(); ?>
</div>