<?php echo "controller = " . yii()->controller->id . " ";
echo "</br>";
echo "action = " . yii()->controller->action->id . " ";
echo "</br>";
echo "c = " . yii()->controller->id . " ";
echo "</br>";
echo "task = item";
echo "</br>";
echo "</br>";
?>
<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'items-form',
	'enableAjaxValidation' => false,
	'htmlOptions' => array(
		'enctype' => 'multipart/form-data'
	),
)); ?>
<?php echo $form->errorSummary($model); ?>
<div class="row">
	<?php echo $form->labelEx($model, 'taskID'); ?>
	<?php echo $form->textField($model, 'taskID'); ?>
	<?php echo $form->error($model, 'taskID'); ?>
</div>
<div class="row">
	<?php echo $form->labelEx($model, 'userID'); ?>
	<?php echo $form->textField($model, 'userID'); ?>
	<?php echo $form->error($model, 'userID'); ?>
</div>
<div class="row">
	<?php echo $form->labelEx($model, 'content'); ?>
	<?php echo $form->textField($model, 'content'); ?>
	<?php echo $form->error($model, 'content'); ?>
</div>
<!--<div class="row">
	<?php /*echo $form->labelEx($model, 'image'); */?>
	<?php /*echo $form->fileField($model, 'image'); */?>
	<?php /*echo $form->error($model, 'image'); */?>
</div>-->
<!--<div class="row">
	<?php /*echo $form->labelEx($model, 'video'); */?>
	<?php /*echo $form->fileField($model, 'video'); */?>
	<?php /*echo $form->error($model, 'video'); */?>
</div>-->
<div class="row buttons">
	<?php echo CHtml::submitButton('Запустить'); ?>
</div>
<?php $this->endWidget(); ?>
</div>
