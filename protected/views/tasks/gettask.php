<?php echo "controller = " . yii()->controller->id . " ";
echo "</br>";
echo "action = " . yii()->controller->action->id . " ";
echo "</br>";
echo "c = " . yii()->controller->id . " ";
echo "</br>";
echo "task = item";
echo "</br>";
echo "</br>";
?>
<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'items-form',
	'enableAjaxValidation' => false,
)); ?>
<?php echo $form->errorSummary($model); ?>
<div class="row">
	<?php echo $form->labelEx($model, 'taskID'); ?>
	<?php echo $form->textField($model, 'taskID'); ?>
	<?php echo $form->error($model, 'taskID'); ?>
</div>
<div class="row">
	<?php echo $form->labelEx($model, 'userID'); ?>
	<?php echo $form->textField($model, 'userID'); ?>
	<?php echo $form->error($model, 'userID'); ?>
</div>
<div class="row buttons">
	<?php echo CHtml::submitButton('Запустить'); ?>
</div>
<?php $this->endWidget(); ?>
</div>
