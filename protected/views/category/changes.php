
<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'items-form',
    'enableAjaxValidation'=>false,
)); ?>

Список в категории (запрос новых/измененных):<br />
iphone.php?c=category&task=changes<br />
Будут перемещены параметры<br />
&category_id=<parent_category_id,default:0><br />
&params=<default:CI    C-отображать категории, I-отображать записи><br />

/*
request data :<br />
{
    parent_category_id: parent_category_id,default<br />
    params: [‘C’,’I’]<br />
}
*/

    <?php echo "controller = " . yii()->controller->id . " ";
    echo "</br>";
    echo "action = " . yii()->controller->action->id . " ";
    echo "</br>";
    echo "c = " . yii()->controller->id . " ";
    echo "</br>";
    echo "task = ".  yii()->controller->action->id . " ";
    echo "</br>";echo "</br>";
    ?>

    <?php echo $form->errorSummary($model); ?>
    <div class="row">
        <?php echo $form->labelEx($model,'category_id'); ?>
        <?php echo $form->textField($model,'category_id',array('size'=>50,'maxlength'=>50,'value'=>'0')); ?>
        <?php echo $form->error($model,'category_id'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'params'); ?>
        <?php echo $form->textField($model,'params',array('size'=>50,'maxlength'=>50,'value'=>'CI')); ?>
        <?php echo $form->error($model,'params'); ?>
    </div>
    <div class="row buttons">
        <?php echo CHtml::submitButton('Запустить'); ?>
    </div>
<?php $this->endWidget(); ?>
</div>