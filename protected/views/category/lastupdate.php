<?=
CHtml::ajaxButton(
	'test_button',
	createUrl(yii()->controller->id . "/" . yii()->controller->action->id),
	array(
		'type' => "POST",
		'data' => array(
			'Category' => array(
				'c' => yii()->controller->id,
				'task' => 'lastUpdateCategory',
				'update_time' => 'js:$("#Category_update_time").val()',
			),
		),
		'success' => "function(data){
			$('.form').append(data);
		}"
	)
)?>

<?php echo "controller = " . yii()->controller->id . " ";
echo "</br>";
echo "action = " . yii()->controller->action->id . " ";
echo "</br>";
echo "c = " . yii()->controller->id . " ";
echo "</br>";
echo "task = lastUpdateCategory ";
echo "</br>";echo "</br>";
?>



<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'items-form',
	'enableAjaxValidation'=>false,
)); ?>


<?php echo $form->errorSummary($model); ?>

<div class="row">
	<?php echo $form->labelEx($model,'update_time'); ?>
	<?php echo $form->textField($model,'update_time',array('size'=>50,'maxlength'=>50,'value'=>'2012-07-30 08:00:40')); ?>
	<?php echo $form->error($model,'update_time'); ?>
</div>

<div class="row buttons">
	<?php echo CHtml::submitButton('Запустить'); ?>
</div>
<?php $this->endWidget(); ?>
</div><br /><br />