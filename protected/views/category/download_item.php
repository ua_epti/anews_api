request data :<br/>
{<br/>
"item_id" : server_id // int<br/>
}<br/>
<br/>
// OK<br/>
response:<br/>
{<br/>
"Result" : “success”<br/>
"Data" : {<br/>
}<br/>
}<br/>
// ERROR<br/>
response:<br/>
{<br/>
"Result" : “error”<br/>
"Message" : message // string.<br/>
}<br/>
<br/>

<?php echo "controller = " . yii()->controller->id . " ";
echo "</br>";
echo "action = " . yii()->controller->action->id . " ";
echo "</br>";
echo "c = " . yii()->controller->id . " ";
echo "</br>";
echo "task = download_item ";
echo "</br>";
echo "</br>";
?>


<div class="form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'items-form',
		'enableAjaxValidation' => false,
	)); ?>
	<?php echo $form->errorSummary($model); ?>
	<div class="row">
		<?php echo $form->labelEx($model, 'item_id'); ?>
		<?php echo $form->textField($model, 'item_id', array('size' => 50, 'maxlength' => 50, 'value' => '38')); ?>
		<?php echo $form->error($model, 'item_id'); ?>
	</div>
	<div class="row buttons">
		<?php echo CHtml::submitButton('Запустить'); ?>
	</div>
	<?php $this->endWidget(); ?>
</div>
