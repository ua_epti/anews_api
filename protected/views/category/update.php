Список в категории (обновление):<br />
iphone.php?&c=category&task=update<br />
Будут перемещены параметры<br />
&cid[]=category_1_id>&cid[]=category_2_id&cid[]=category_2_id<br />
&iid[]=item_1_id&iid[]=item_1_id&iid[]=item_1_id<br />
&params=default:CI    C-отображать категории, I-отображать записи<br />
<br />
/*<br />
request data :<br />
{<br />
    category_id: [category_id, category_id, category_id]<br />
    item_id: []<br />
    params: [‘C’,’I’]<br />
}<br />
*/<br />

// OK<br />

<?php echo "controller = " . yii()->controller->id . " ";
echo "</br>";
echo "action = " . yii()->controller->action->id . " ";
echo "</br>";
echo "c = " . yii()->controller->id . " ";
echo "</br>";
echo "task = ".  yii()->controller->action->id . " ";
echo "</br>";echo "</br>";
?>

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'items-form',
    'enableAjaxValidation'=>false,
)); ?>


    <?php echo $form->errorSummary($model); ?>
    <div class="row">
        <?php echo $form->labelEx($model,'category_ids'); ?>
        <?php echo $form->textField($model,'category_ids',array('size'=>50,'maxlength'=>50,'value'=>'1,2,3,8,13,25,26,27,400')); ?>
        <?php echo $form->error($model,'category_ids'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'item_ids'); ?>
        <?php echo $form->textField($model,'item_ids',array('size'=>50,'maxlength'=>50,'value'=>'1,2,3,8,10,44,43,46,400')); ?>
        <?php echo $form->error($model,'item_ids'); ?>
    </div>
    
    <div class="row">
        <?php echo $form->labelEx($model,'params'); ?>
        <?php echo $form->textField($model,'params',array('size'=>50,'maxlength'=>50,'value'=>'CI')); ?>
        <?php echo $form->error($model,'params'); ?>
    </div>
    <div class="row buttons">
        <?php echo CHtml::submitButton('Запустить'); ?>
    </div>
<?php $this->endWidget(); ?>
</div><br /><br />