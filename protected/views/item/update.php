<?php
$this->breadcrumbs=array(
    'Тренки'=>array('list'),
    $model->title=>array('view','id'=>$model->id),
    'Редактирвоание',
);

$this->widget('zii.widgets.CMenu',array(
    'items'=>array(
        array('label'=>'Создать итем', 'url'=>array('/item/create')),
    ),
)); ?>

<h1>Тренк  <?php echo $model->title; ?></h1>
<?php echo $this->renderPartial('_form', array('model'=>$model,'trenks'=>$trenks)); ?>

