<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'login-form',
    'enableClientValidation'=>true,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    ),
)); ?>

<?if(extension_loaded('gd') && Yii::app()->user->isGuest):?>
    <?=CHtml::activeLabelEx($model, 'verifyCode')?>
    <?$this->widget('CCaptcha')?>
    <?=CHtml::activeTextField($model, 'verifyCode')?>
<?endif?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>
    <div class="row">
        <?php echo $form->labelEx($model,'id_category'); ?>
        <?php echo $form->textField($model,'id_category'); ?>
        <?php echo $form->error($model,'id_category'); ?>
    </div>
    <div class="row">
       <?php echo $form->labelEx($model,'id_trenk'); ?>
       <?php echo CHtml::activeDropDownList($model,'id_trenk',CHtml::listData($trenks, 'id','title'),array('selected'=>$model->id_trenk)); ?>
       <?php echo $form->error($model,'id_trenk'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'title'); ?>
        <?php echo $form->textField($model,'title'); ?>
        <?php echo $form->error($model,'title'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'description'); ?>
        <?php echo $form->textField($model,'description'); ?>
        <?php echo $form->error($model,'description'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'id_image'); ?>
        <?php echo $form->textField($model,'id_image'); ?>
        <?php echo $form->error($model,'id_image'); ?>
    </div>
    
    
    <div class="row buttons">
        <?php echo CHtml::submitButton('Сохранить'); ?>
    </div>

<?php $this->endWidget(); ?>
</div><!-- form -->
