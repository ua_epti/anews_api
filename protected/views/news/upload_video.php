<div class="form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'login-form',
		'action' => Yii::app()->params['connectUrl'] . '?c=user&task=upVideo',
		'enableClientValidation' => true,
		'htmlOptions' => array('enctype' => 'multipart/form-data'),
		'clientOptions' => array(
			'validateOnSubmit' => true,
		),

	)); ?>

	<div class="row">
		<?php echo $form->labelEx($model, 'userID'); ?>
		<?php echo $form->textField($model, 'userID', array('value' => 5)); ?>
		<?php echo $form->error($model, 'userID'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model, 'newsID'); ?>
		<?php echo $form->textField($model, 'newsID', array('value' => 29)); ?>
		<?php echo $form->error($model, 'newsID'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model, 'video'); ?>
		<?php echo $form->fileField($model, 'video'); ?>
		<?php echo $form->error($model, 'video'); ?>
	</div>
	<div class="row buttons">
		<?php echo CHtml::submitButton('Послать'); ?>
	</div>

	<?php $this->endWidget(); ?>
</div><!-- form -->
