<?/***
 * @var $form CActiveForm
 */
?>
<div class="form">
	<? $form = $this->beginWidget('CActiveForm', array(
		'id' => 'items-form',
		'enableAjaxValidation' => false,
	)); ?>
	<?= $form->errorSummary($model); ?>
	<div class="row">
		<?= $form->labelEx($model, 'userID'); ?>
		<?= $form->textField($model, 'userID'); ?>
		<?= $form->error($model, 'userID'); ?>
	</div>
	<div class="row">
		<?= $form->labelEx($model, 'is_avaliable'); ?>
		<?= $form->checkBox($model, 'is_avaliable'); ?>
		<?= $form->error($model, 'is_avaliable'); ?>
	</div>
	<div class="row buttons">
		<?= CHtml::submitButton('Ок')
		/*CHtml::ajaxButton(
			'test_button',
			createUrl(yii()->controller->id . "/" . yii()->controller->action->id),
			array(
				'type' => "POST",
				'data' => array(
					'News' => array(
						'c' => yii()->controller->id,
						'task' => 'mynews',
						'timeUpdate' => 'js:$("#News_timeUpdate").val()',
					),
				),
				'success' => "function(data){
			$('.form').append(data);
		}"
			)
		)*/?>
	</div>
	<?php $this->endWidget(); ?>
</div>