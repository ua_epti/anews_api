<?php echo "controller = " . yii()->controller->id . " ";
echo "</br>";
echo "action = " . yii()->controller->action->id . " ";
echo "</br>";
echo "c = " . yii()->controller->id . " ";
echo "</br>";
echo "task = upload_avatar";
echo "</br>";
echo "</br>";
?>
<?/***
 * @var $form CActiveForm
 */
?>


<?php /* 
echo CHtml::ajaxButton(
	'test_button',
	createUrl(yii()->controller->id . "/" . "uploadImage"),
	array(
		'type' => "POST",
		'data' => array(
			'Userinfo' => array(
				'c' => yii()->controller->id,
				'task' => 'upload_image',
				'image' => 'js:($("#Tasks_image").val())',
				'userID' => 'js:parseInt($("#Tasks_userID").val())',
			),
		),
		'success' => "function(data){
    $('.form').append(data);
   }"
	),
	array(
		'id' => 'ajaxButton',
	)
)*/
?>

<div class="form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'login-form',
//		'action' => Yii::app()->params['connectUrl'] . '?c=user&task=upImage',
		'enableClientValidation' => true,
		'htmlOptions' => array('enctype' => 'multipart/form-data'),
		'clientOptions' => array(
			'validateOnSubmit' => true,
		),

	)); ?>

	<div class="row">
		<?php echo $form->labelEx($model, 'userID'); ?>
		<?php echo $form->textField($model, 'userID', array('value' => 5)); ?>
		<?php echo $form->error($model, 'userID'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model, 'newsID'); ?>
		<?php echo $form->textField($model, 'newsID', array('value' => 29)); ?>
		<?php echo $form->error($model, 'newsID'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model, 'image'); ?>
		<?php echo $form->fileField($model, 'image'); ?>
        <?php echo $form->error($model, 'image'); ?>
	</div>

	<div class="row">
		<? //php echo $form->labelEx($model,'avatar'); ?>
		<? //php echo $form->fileField($model,'avatar'); ?>
		<? //php echo $form->error($model,'avatar'); ?>
	</div>
	<div class="row buttons">
		<?php echo CHtml::submitButton('Послать'); ?>
	</div>

	<?php $this->endWidget(); ?>
</div><!-- form -->
