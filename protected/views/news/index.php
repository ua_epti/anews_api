<div class="form">
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id' => 'items-form',
		'enableAjaxValidation' => false,
	)); ?>
	<?php echo $form->errorSummary($model); ?>
	<div class="row">
		<?php echo $form->labelEx($model, 'timeCreate'); ?>
		<?php echo $form->textField($model, 'timeCreate', array('size' => 50, 'maxlength' => 50, 'value' => yii()->dateFormatter->formatDateTime(time()))); ?>
		<?php echo $form->error($model, 'timeCreate'); ?>
	</div>
	<div class="row buttons">
		<?php echo CHtml::submitButton('Запустить'); ?>
	</div>
	<?php $this->endWidget(); ?>
</div>