<div class="form">
<?php
$form=$this->beginWidget('CActiveForm', array(
    'id'=>'feedback-form',
    'enableClientValidation'=>true,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    ),
    
    ));

 ?>
 
<?php  echo Yii::app()->user->getFlash('feedback'); ?>
<p class="note">Поля, помеченные <span class="required">*</span> обязательны.</p>

<?php echo $form->errorSummary($model); ?>
    
<?php if (Yii::app()->user->isGuest) {?>
    <div class="row">
        <?php echo $form->labelEx($model,'first_name'); ?>
        <?php echo $form->textField($model,'first_name',array('size'=>50,'maxlength'=>50)); ?>
        <?php echo $form->error($model,'first_name'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'last_name'); ?>
        <?php echo $form->textField($model,'last_name',array('size'=>50,'maxlength'=>50)); ?>
        <?php echo $form->error($model,'last_name'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'email'); ?>
        <?php echo $form->textField($model,'email',array('size'=>50,'maxlength'=>50)); ?>
        <?php echo $form->error($model,'email'); ?>
    </div>
<?php
}
?>
    <div class="row">
        <?php echo $form->labelEx($model,'category'); ?>
        <?php echo CHtml::activeDropDownList($model,'category',CHtml::listData($feedbackCategory, 'id','category')); ?>    
        <?php echo $form->error($model,'category'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'importance'); ?>
        <?php //echo $form->textField($model,'importance');
 echo $form->dropDownList($model,'importance',
array('1'=>1, '2'=>2, '3'=>3, '4'=>4, '5'=>5, '6'=>6, '7'=>7, '8'=>8, '9'=>9, '10'=>10)); 
        ?>
        <?php echo $form->error($model,'importance'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'question'); ?>
        <?php echo $form->textArea($model,'question'); ?>
        <?php echo $form->error($model,'question'); ?>
    </div>
    <div class="row buttons">
        <?php echo CHtml::submitButton( 'Отправить'); ?>
    </div>
<?php $this->endWidget(); ?>

</div><!-- form -->