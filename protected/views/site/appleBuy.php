<?php echo "controller = " . yii()->controller->id . " ";
echo "</br>";
echo "action = " . yii()->controller->action->id . " ";
echo "</br>";
echo "c = " . yii()->controller->id . " ";
echo "</br>";
echo "task = ".yii()->controller->action->id. " ";
echo "</br>";echo "</br>";
?>



<?=
CHtml::ajaxButton(
	'test_button',
	createUrl(yii()->controller->id . "/" . yii()->controller->action->id),
	array(
		'type' => "POST",
		'data' => array(
			'Category' => array(
				'c' => yii()->controller->id,
				'task' => yii()->controller->action->id,
				'inappID' => 'js:($("#Audioguides_inappID").val())',
				'audioguides' => 'js:parseInt($("#Audioguides_audioguides").val())',
				'user_id' => 'js:parseInt($("#LoginForm_user_id").val())',
			),
		),
		'success' => "function(data){
    $('.form').append(data);
   }"
	)
)?>

<div class="form">
    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'audioguides-form',
        'action' => 'appleBuy',
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
        ),
    )); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

	<div class="row">
		<?php //echo $form->labelEx($modelAudioguides, 'audioguides');
		echo 'id_audioguide';?>
		<br>
		<?php echo $form->textField($modelAudioguides, 'audioguides'); ?>

	</div>

	<div class="row">
		<?php //echo $form->labelEx($modelAudioguides, 'inappID');
		echo 'inappID';?>
		<br>
		<?php echo $form->textField($modelAudioguides, 'inappID'); ?>

	</div>

	<div class="row">
		<?php echo $form->labelEx($modelUser, 'user_id'); ?>
		<?php echo $form->textField($modelUser, 'user_id'); ?>

	</div>

    <div class="row buttons">
        <?php echo CHtml::submitButton('Send'); ?>
    </div>

    <?php $this->endWidget(); ?>
</div>

