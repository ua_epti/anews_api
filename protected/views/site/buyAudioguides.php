<?php echo "controller = site";
echo "</br>";
echo "action = buyAudioguides";
echo "</br>";
echo "c = " .'audioguides' . " ";
echo "</br>";
echo "task = buy";
echo "</br>";
?>


<div class="form">
    <?php $form = $this->beginWidget('CActiveForm', array(
        'id' => 'audioguides-form',
        'action' => 'buyAudioguides',
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
        ),
    )); ?>



    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <div class="row">
        <?php echo $form->labelEx($modelUser, 'username'); ?>
        <?php echo $form->textField($modelUser, 'username'); ?>
        <?php echo $form->error($modelUser, 'username'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($modelUser, 'password'); ?>
        <?php echo $form->passwordField($modelUser, 'password'); ?>
        <?php echo $form->error($modelUser, 'password'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($modelAudioguides, 'audioguides'); ?>
        <?php echo $form->passwordField($modelAudioguides, 'audioguides'); ?>

    </div>

    <div class="row buttons">
        <?php echo CHtml::submitButton('Send'); ?>
    </div>



    <?php $this->endWidget(); ?>
</div>

