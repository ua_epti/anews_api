<?php
if( Yii::app()->user->checkAccess('administrator') )
{
    $this->breadcrumbs=array(
        'Пользователи'=>array('index'),
        'Регистрация нового тренкера',
    );
}
else
{
    $this->breadcrumbs=array(
        'Регистрация',
    );
}

?>

<p>Пожалуйста заполните форму своими данными:</p>

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'login-form',
    'enableClientValidation'=>true,
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    ),
)); ?>

    <p class="note">Поля отмеченные <span class="required">*</span> обязательны к заполнению.</p>

    <div class="row">
        <?php echo $form->labelEx($model,'first_name'); ?>
        <?php echo $form->textField($model,'first_name'); ?>
        <?php echo $form->error($model,'first_name'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'last_name'); ?>
        <?php echo $form->textField($model,'last_name'); ?>
        <?php echo $form->error($model,'last_name'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'email'); ?>
        <?php echo $form->textField($model,'email'); ?>
        <?php echo $form->error($model,'email'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'password'); ?>
        <?php echo $form->passwordField($model,'password'); ?>
        <?php echo $form->error($model,'password'); ?>
    </div>
    <div class="row">
        <?php echo $form->labelEx($model,'id_category'); ?>
        <?php echo CHtml::activeDropDownList($model,'id_category',CHtml::listData($usersCategory , 'id','title')); ?>
        <?php echo $form->error($model,'id_category'); ?>
    </div>

    <div class="row">
        <?php echo $form->labelEx($model,'gender'); ?>
        <?php echo CHtml::dropDownList('SingupForm[gender]', $model->gender,
              array('1' => 'Мужской', '2' => 'Женский'));
        ?>
    </div>
    
    <div class="row buttons">
    <?php echo CHtml::submitButton('SingUp'); ?>    
    </div>

<?php $this->endWidget(); ?>
</div><!-- form -->