<?php $this->pageTitle = Yii::app()->name; ?>
<?php $this->widget('zii.widgets.CMenu', array(
	'items' => array(
		array('label' => 'Регистарция пользователя', 'url' => array('/user/signup')),
		array('label' => 'Авторизация пользователя', 'url' => array('/user/login')),
		array('label' => 'Выход пользователя', 'url' => array('/user/logout')),
		array('label' => 'Востановить пароль', 'url' => array('/user/remember')),

		array('label' => 'Информация пользователей ', 'url' => array('/user/userinfo')),
//		array('label' => 'Информация пользователей (запрос изменений) ', 'url' => array('/user/changes')),
//		array('label' => 'Обновить свое инфо пользователя ', 'url' => array('/user/update')),
//		array('label' => 'Аплоад аватарки пользователя ', 'url' => array('/user/uploadavatar')),
	),
)); ?>


<?php $this->widget('zii.widgets.CMenu', array(
	'items' => array(
		array('label' => 'Задачи общий список', 'url' => array('/tasks/index')),
		array('label' => 'Взять таск в работу', 'url' => array('/tasks/getTaskByUser')),
		array('label' => 'Выполнить таск', 'url' => array('/tasks/complete')),
//		array('label' => 'Последняя обновленная категория', 'url' => array('/category/lastupdate')),
//		array('label' => 'Список купленных гидов', 'url' => array('/category/purchased')),
//		array('label' => 'Пользователь скачал файлы итема', 'url' => array('/category/downloadItem')),
//
	),
));


$this->widget('zii.widgets.CMenu', array(
	'items' => array(
		array('label' => 'Новости общий список', 'url' => array('/news/index')),
		array('label' => 'Мои новости', 'url' => array('/news/mynews/')),
		array('label' => 'Загрузка Фото', 'url' => array('/news/uploadimage/')),
		array('label' => 'Загрузка Видео', 'url' => array('/news/uploadvideo/')),
	),
));
?>

	<!--	<h3>Новости - Битва мастеров</h3>-->
<?php //$this->widget('zii.widgets.CMenu', array(
//	'items' => array(
//		array('label' => 'Новости', 'url' => array('/news/index')),
//		array('label' => 'Новости обновленные', 'url' => array('/news/update')),
//		array('label' => 'Лайк новости', 'url' => array('/news/like')),
//	),
//));
?>