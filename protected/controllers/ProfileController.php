<?php
class ProfileController extends Controller
{

    public function actions()
    {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha'=>array(
                'class'=>'CCaptchaAction',
                'backColor'=>0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page'=>array(
                'class'=>'CViewAction',
            ),
        );
    }

    

    public function actionError()
    {
        if($error=Yii::app()->errorHandler->error)
        {
            if(Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
        }

    
   public function actionIndex(){    
        
        $urlGetUser=Yii::app()->params['connectUrl'].'/view?code='.Yii::app()->session['code'].'&model=users'; 
        $resultUser = post_content($urlGetUser,'');
        $user=json_decode($resultUser['content']);
        $urlGetProfile=Yii::app()->params['connectUrl'].'/view?code='.Yii::app()->session['code'].'&model=profile&id_user='.$user->id_user; 
        $resultProfile=post_content($urlGetProfile,'');
        $models=json_decode($resultProfile['content']);
       
        $this->render('index',array('models'=>$models));
        
    } 
    
   public function actionUpdate(){
        echo "Вытяуть каким либо способом поля пользователя";
        $urlGetUser=Yii::app()->params['connectUrl'].'/view?code='.Yii::app()->session['code'].'&model=users'; 
        $resultUser = post_content($urlGetUser,'');
        $user=json_decode($resultUser['content']);
        
        $urlGetProfileFields=Yii::app()->params['connectUrl'].'/list?code='.Yii::app()->session['code'].'&model=profile_fields&id_category='.$user->id_category; 
        
        $resultProfileFields = post_content($urlGetProfileFields,'');
        
        
        $profileFields=json_decode($resultProfileFields['content']);
        
        $this->render('update',
            array(
            'models'=>$profileFields,
            )
        );
        
        
   }
    
  
    
    /*
    public function actionList($id){    
        $url=Yii::app()->params['connectUrl'].'/list?code='.Yii::app()->session['code'].'&model=ProfilesByTrenkId&id_trenk='.$id; 
        $result = post_content($url,'');
        //dump($result);
        
        if ($result['http_code']==200)
        {
            $Profiles=json_decode($result['content']);
            $this->render('list',array(
                    'Profiles'=>$Profiles,
            ));
        }
        else
        {
            $this->render('list',array('error'=>$result['content'],));
        }
    }
    
    public function actionView($id)
    {
            $url=Yii::app()->params['connectUrl'].'/view?code='.Yii::app()->session['code'].'&model=Profiles&id='.$id; 
            $result = post_content($url,'');
            
            
            if ($result['http_code']==200)
            {
                $Profile=json_decode($result['content']);
                $this->render('view',array(
                    'Profile'=>$Profile,
                ));
            }
            else
            {
                $this->render('view',array('error'=>$result['content'],));
            }
 
    }
    public function actionCreate(){
       
       
        $url=Yii::app()->params['connectUrl'].'/list?code='.Yii::app()->session['code'].'&model=trenks'; 
        $result = post_content($url,'');
        
        if ($result['http_code']!=200)
        {
            echo $result['content'];
            exit;
        }
        
        $trenks=json_decode($result['content']);
        
        
        
        $model=new ProfileForm;
        if(isset($_POST['ProfileForm']))
        {                                                     
            $id_trenk=$_POST['ProfileForm']['id_trenk'];
            unset($_POST['ProfileForm']['id_trenk']);
            
            $url=Yii::app()->params['connectUrl'].'/create?code='.Yii::app()->session['code'].'&model=Profiles'; 
            $result = post_content($url,$_POST['ProfileForm']);
            $return_Profile=json_decode($result['content']);
            
            $_POST['TrenksData']['id_trenk']=$id_trenk;
            $_POST['TrenksData']['id_Profile']=$return_Profile->id;
            
            $url2=Yii::app()->params['connectUrl'].'/create?code='.Yii::app()->session['code'].'&model=trenks_data'; 
            $trenks_data = post_content($url2,$_POST['TrenksData']);
            
            $this->redirect(array('Profile/list&id='.$id_trenk));
        }

        $this->render('create',array(
            'model'=>$model,
            'trenks'=>$trenks,
        ));
        
    }
    
    
    public function actionUpdate($id){
        $trenk_url=Yii::app()->params['connectUrl'].'/list?code='.Yii::app()->session['code'].'&model=trenks'; 
        $return_trenk = post_content($trenk_url,'');
        
        if ($return_trenk['http_code']!=200)
        {
            echo $return_trenk['content'];
            exit;
        }
        
         $trenks=json_decode($return_trenk['content']);   
         $url=Yii::app()->params['connectUrl'].'/view?code='.Yii::app()->session['code'].'&model=Profiles&id='.$id; 
         $result = post_content($url,'');
         if ($result['http_code']!=200){
            $this->render('update',array('error'=>$result['content'],));  
         }else{
            $Profile=json_decode($result['content']);    
         
            $model=new ProfileForm;
            $model->id=$Profile->id;
            $model->id_trenk=$Profile->id_trenk;
            $model->id_category=$Profile->id_category;
            $model->title=$Profile->title;
            $model->description=$Profile->description;
            $model->id_image=$Profile->id_image;
                if(isset($_POST['ProfileForm']))
                {                                                     
                    
                    $id_trenk=$_POST['ProfileForm']['id_trenk'];
                    unset($_POST['ProfileForm']['id_trenk']);
                    
                    $url_Profile=Yii::app()->params['connectUrl'].'/update?code='.Yii::app()->session['code'].'&model=Profiles&id='.$Profile->id; 
                    $return_Profile = post_content($url_Profile,$_POST['ProfileForm']);
                    
                    $url_trenkData=Yii::app()->params['connectUrl'].'/view?code='.Yii::app()->session['code'].'&model=tranks_dataByProfileId&id_Profile='.$Profile->id;;                                      $return_trenkData = post_content($url_trenkData,'');
                    $trenkData=json_decode($return_trenkData['content']);
                    
                    $_POST['TrenksData']['id_trenk']=$id_trenk;
                    $_POST['TrenksData']['id_Profile']=$Profile->id;
                    
                    $url=Yii::app()->params['connectUrl'].'/update?code='.Yii::app()->session['code'].'&model=trenks_data&id='.$trenkData->id; 
                    $update_trenkData = post_content($url,$_POST['TrenksData']);
                    
                    $this->redirect(array('view','id'=>$model->id));
                }
            
            
            $this->render('update',array(
                'model'=>$model,
                'trenks'=>$trenks,
            )); 
         }
    }
    
    public function actionDelete($id){
            
            
            $url_trenkData=Yii::app()->params['connectUrl'].'/view?code='.Yii::app()->session['code'].'&model=tranks_dataByProfileId&id_Profile='.$id;;                                      
            $return_trenkData = post_content($url_trenkData,'');
            $trenkData=json_decode($return_trenkData['content']);
            
            $url_delTrenkData=Yii::app()->params['connectUrl'].'/delete?code='.Yii::app()->session['code'].'&model=trenks_data&id='.$trenkData->id; 
            $deletetTrenkData = post_content($url_delTrenkData,'');
            
            $url=Yii::app()->params['connectUrl'].'/delete?code='.Yii::app()->session['code'].'&model=Profiles&id='.$id; 
            $deleteProfile = post_content($url,'');
            
            $this->redirect(array('index'));
            
    }
    */
    
    

}