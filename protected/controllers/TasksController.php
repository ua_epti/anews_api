<?
class TasksController extends Controller
{
	public function actions()
	{
		return array(
			'captcha' => array(
				'class' => 'CCaptchaAction',
				'backColor' => 0xFFFFFF,
			),
			'page' => array(
				'class' => 'CViewAction',
			),
		);
	}

	public function actionIndex()
	{
		$url = Yii::app()->params['connectUrl'] . '/?c=tasks&task=all';
		//dump($url);die;
		$result = post_content($url, $_POST['Tasks']);
		//dump(CHtml::decode($result['content']));die;
		echo CHtml::decode($result['content']);
		exit;
	}

	public function actionGetTaskByUser()
	{
		$model = new Tasks();
		if (isset($_POST['Tasks'])) {
			$url = Yii::app()->params['connectUrl'] . '/?c=gettasks&task=byuser';
			$result = post_content($url, $_POST['Tasks']);
			echo CHtml::decode($result['content']);
			exit;
		}
		$this->render('gettask', array('model' => $model));

	}

	public function actionComplete()
	{
		$model = new Tasks('complete');
		if (isset($_POST['Tasks'])) {
			$_POST['Tasks']['image'] = $_FILES['Tasks']['name']['image'];
//			$_POST['Tasks']['image'] = 'data:image/gif;base64,' . $imgBinary;
			$url = Yii::app()->params['connectUrl'] . '/?c=task&task=complete';
			$result = post_content($url, $_POST['Tasks']);
			echo CHtml::decode($result['content']);
			exit;
		}
		$this->render('complete', array('model' => $model));
	}



	/*public function actionUploadVideo()
	{
		$model = new Tasks();
		if (isset($_POST['Tasks'])) {
			/*if (isset($_FILES)) {
				dump($_FILES);die;
				$_POST['Tasks']['image'] =
			}*/
	/*$imgBinary = $_POST['Tasks']['image'];
	$url = Yii::app()->params['connectUrl'] . '?c=user&task=upImage';
	$_POST['Tasks']['image'] = 'data:image/gif;base64,' . $imgBinary;
	$result = post_content($url, $_POST['Tasks']);
	echo CHtml::decode($result['content']);
	exit;*/
//			} else {
//				echo "Вы не выбрали файл";
//				exit;
//			}
//		}
	/*$this->render('upload_image', array(
		'model' => $model,
	));
}*/


	/*function base64_encode_image($imagefile)
		{
			$imgtype = array('jpg', 'gif', 'png');
			$filename = file_exists($imagefile) ? htmlentities($imagefile) : die('Image file name does not exist');
			$filetype = pathinfo($filename, PATHINFO_EXTENSION);
			if (in_array($filetype, $imgtype)) {
				$imgbinary = fread(fopen($filename, "r"), filesize($filename));
			} else {
				die ('Invalid image type, jpg, gif, and png is only allowed');
			}
			return 'data:image/' . $filetype . ';base64,' . base64_encode($imgbinary);
		}*/

	/*public function actionView()
	{
		$model = new Blog();
		if (isset($_POST['Blog'])) {
			$url = Yii::app()->params['connectUrl'] . '/?c=blog&task=item';
			$result = post_content($url, $_POST['Blog']);
			// print($result['content']);
			echo CHtml::decode($result['content']);
			exit;
		}
		$this->render('view', array('model' => $model));
	}*/

	public function actionError()
	{
		if ($error = Yii::app()->errorHandler->error) {
			if (Yii::app()->request->isAjaxRequest) {
				echo $error['message'];
			} else {
				$this->render('error', $error);
			}
		}
	}
}