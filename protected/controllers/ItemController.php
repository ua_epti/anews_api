<?php

class ItemController extends Controller
{

    public function actions()
    {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha'=>array(
                'class'=>'CCaptchaAction',
                'backColor'=>0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page'=>array(
                'class'=>'CViewAction',
            ),
        );
    }

    
   /* public function actionIndex()
    {
        $this->render('index');
    }
            */
    public function actionError()
    {
        if($error=Yii::app()->errorHandler->error)
        {
            if(Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    
   public function actionIndex(){    
        $url=Yii::app()->params['connectUrl'].'/list?code='.Yii::app()->session['code'].'&model=items'; 
        $result = post_content($url,'');
        if ($result['http_code']==200)
        {
            $items=json_decode($result['content']);
            $this->render('index',array(
                    'items'=>$items,
            ));
        }
        else
        {
            $this->render('index',array('error'=>$result['content'],));
        }
    } 
    
  
    
    public function actionList($id){    
        $url=Yii::app()->params['connectUrl'].'/list?code='.Yii::app()->session['code'].'&model=itemsByTrenkId&id_trenk='.$id; 
        $result = post_content($url,'');
        //dump($result);
        
        if ($result['http_code']==200)
        {
            $items=json_decode($result['content']);
            $this->render('list',array(
                    'items'=>$items,
            ));
        }
        else
        {
            $this->render('list',array('error'=>$result['content'],));
        }
    }
    
    public function actionView($id)
    {
            $url=Yii::app()->params['connectUrl'].'/view?code='.Yii::app()->session['code'].'&model=items&id='.$id; 
            $result = post_content($url,'');
            
            
            if ($result['http_code']==200)
            {
                $item=json_decode($result['content']);
                $this->render('view',array(
                    'item'=>$item,
                ));
            }
            else
            {
                $this->render('view',array('error'=>$result['content'],));
            }
 
    }
    public function actionCreate(){
       
       
        $url=Yii::app()->params['connectUrl'].'/list?code='.Yii::app()->session['code'].'&model=trenks'; 
        $result = post_content($url,'');
        
        if ($result['http_code']!=200)
        {
            echo $result['content'];
            exit;
        }
        
        $trenks=json_decode($result['content']);
        
        
        
        $model=new ItemForm;
        if(isset($_POST['ItemForm']))
        {                                                     
            $id_trenk=$_POST['ItemForm']['id_trenk'];
            unset($_POST['ItemForm']['id_trenk']);
            
            $url=Yii::app()->params['connectUrl'].'/create?code='.Yii::app()->session['code'].'&model=items'; 
            $result = post_content($url,$_POST['ItemForm']);
            $return_item=json_decode($result['content']);
            
            $_POST['TrenksData']['id_trenk']=$id_trenk;
            $_POST['TrenksData']['id_item']=$return_item->id;
            
            $url2=Yii::app()->params['connectUrl'].'/create?code='.Yii::app()->session['code'].'&model=trenks_data'; 
            $trenks_data = post_content($url2,$_POST['TrenksData']);
            
            $this->redirect(array('item/list&id='.$id_trenk));
        }

        $this->render('create',array(
            'model'=>$model,
            'trenks'=>$trenks,
        ));
        
    }
    
    
    public function actionUpdate($id){
        $trenk_url=Yii::app()->params['connectUrl'].'/list?code='.Yii::app()->session['code'].'&model=trenks'; 
        $return_trenk = post_content($trenk_url,'');
        
        if ($return_trenk['http_code']!=200)
        {
            echo $return_trenk['content'];
            exit;
        }
        
         $trenks=json_decode($return_trenk['content']);   
         $url=Yii::app()->params['connectUrl'].'/view?code='.Yii::app()->session['code'].'&model=items&id='.$id; 
         $result = post_content($url,'');
         if ($result['http_code']!=200){
            $this->render('update',array('error'=>$result['content'],));  
         }else{
            $item=json_decode($result['content']);    
         
            $model=new ItemForm;
            $model->id=$item->id;
            $model->id_trenk=$item->id_trenk;
            $model->id_category=$item->id_category;
            $model->title=$item->title;
            $model->description=$item->description;
            $model->id_image=$item->id_image;
                if(isset($_POST['ItemForm']))
                {                                                     
                    
                    $id_trenk=$_POST['ItemForm']['id_trenk'];
                    unset($_POST['ItemForm']['id_trenk']);
                    
                    $url_item=Yii::app()->params['connectUrl'].'/update?code='.Yii::app()->session['code'].'&model=items&id='.$item->id; 
                    $return_item = post_content($url_item,$_POST['ItemForm']);
                    
                    $url_trenkData=Yii::app()->params['connectUrl'].'/view?code='.Yii::app()->session['code'].'&model=tranks_dataByItemId&id_item='.$item->id;;                                      $return_trenkData = post_content($url_trenkData,'');
                    $trenkData=json_decode($return_trenkData['content']);
                    
                    $_POST['TrenksData']['id_trenk']=$id_trenk;
                    $_POST['TrenksData']['id_item']=$item->id;
                    
                    $url=Yii::app()->params['connectUrl'].'/update?code='.Yii::app()->session['code'].'&model=trenks_data&id='.$trenkData->id; 
                    $update_trenkData = post_content($url,$_POST['TrenksData']);
                    
                    $this->redirect(array('view','id'=>$model->id));
                }
            
            
            $this->render('update',array(
                'model'=>$model,
                'trenks'=>$trenks,
            )); 
         }
    }
    
    public function actionDelete($id){
            
            
            $url_trenkData=Yii::app()->params['connectUrl'].'/view?code='.Yii::app()->session['code'].'&model=tranks_dataByItemId&id_item='.$id;;                                      
            $return_trenkData = post_content($url_trenkData,'');
            $trenkData=json_decode($return_trenkData['content']);
            
            $url_delTrenkData=Yii::app()->params['connectUrl'].'/delete?code='.Yii::app()->session['code'].'&model=trenks_data&id='.$trenkData->id; 
            $deletetTrenkData = post_content($url_delTrenkData,'');
            
            $url=Yii::app()->params['connectUrl'].'/delete?code='.Yii::app()->session['code'].'&model=items&id='.$id; 
            $deleteItem = post_content($url,'');
            
            $this->redirect(array('index'));
            
    }
    
    
    

}