<?
class UserController extends Controller
{
	public function actions()
	{
		return array(
			'captcha' => array(
				'class' => 'CCaptchaAction',
				'backColor' => 0xFFFFFF,
			),
			'page' => array(
				'class' => 'CViewAction',
			),
		);
	}


	public function actionIndex()
	{
		$this->render('index');
	}

	public function actionError()
	{
		if ($error = Yii::app()->errorHandler->error) {
			if (Yii::app()->request->isAjaxRequest) {
				echo $error['message'];
			} else {
				$this->render('error', $error);
			}
		}
	}

	public function actionLogin()
	{
		$model = new LoginForm;
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
		if (isset($_POST['LoginForm'])) {
			$_POST['LoginForm']['email'] = $_POST['LoginForm']['username'];
			$url = Yii::app()->params['connectUrl'] . '?c=user&task=login';
			$result = post_content($url, $_POST['LoginForm']);
			echo CHtml::decode($result['content']);
			exit;
		}
		$this->render('login', array(
			'model' => $model,
		));
	}

	public function actionSignup()
	{
		$model = new SingupForm;
		if (isset($_POST['SingupForm'])) {
			$_POST['SingupForm']['last_online'] = $_POST['SingupForm']['reg_time'] = time();
			$url = Yii::app()->params['connectUrl'] . '?c=user&task=registration';
			$result = post_content($url, $_POST['SingupForm']);
			echo $result['content'];
//			dump($result);
			yii()->end();
		}
		$this->render('signup', array(
			'model' => $model,
		));
	}

	public function actionLogout()
	{
		$model = new Logout;
		if (isset($_POST['Logout'])) {
			$url = Yii::app()->params['connectUrl'] . '?c=user&task=logout';
			$result = post_content($url, $_POST['Logout']);
			/* dump($result['content']);
				dump(json_decode($result['content']));*/
			echo CHtml::encode($result['content']);
			exit;
		}
		$this->render('logout', array(
			'model' => $model,
		));
	}

	public function actionUserinfo()
	{
		$model = new Userinfo;
		if (isset($_POST['Userinfo'])) {
			$url = Yii::app()->params['connectUrl'] . '?c=user&task=userinfo';
			$result = post_content($url, $_POST['Userinfo']);
			// dump($result['content']);
			//dump(json_decode($result['content']));
			echo CHtml::decode($result['content']);
			exit;
		}
		$this->render('userinfo', array(
			'model' => $model,
		));

	}

	public function actionUpdate()
	{
		$model = new Userinfo;
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
		if (isset($_POST['Userinfo'])) {
			$url = Yii::app()->params['connectUrl'] . '?c=user&task=update_info';
			$result = post_content($url, $_POST['Userinfo']);
			/* dump($result['content']);
				dump(json_decode($result['content']));*/
			echo CHtml::decode($result['content']);
			exit;
		}
		$this->render('update', array(
			'model' => $model,
		));


	}

	public function actionRemember()
	{
		$model = new Userinfo;
		if (isset($_POST['Userinfo'])) {
			$url = Yii::app()->params['connectUrl'] . '?c=user&task=remember';
			$result = post_content($url, $_POST['Userinfo']);
			echo CHtml::decode($result['content']);
			exit;
		}
		$this->render('remember', array(
			'model' => $model,
		));

	}

	public function actionUploadavatar()
	{
		$model = new Userinfo;
		if (isset($_POST['Userinfo'])) {
			if (($_POST['Userinfo']['avatar']) != "") {
				$imgbinary = $_POST['Userinfo']['avatar'];
				$url = Yii::app()->params['connectUrl'] . '?c=user&task=upload_avatar';
				//todo:уточнить принимаемый формат.КТо конвертирует!?
				$_POST['Userinfo']['image_base64'] = 'data:image/gif;base64,' . $imgbinary;
				$result = post_content($url, $_POST['Userinfo']);
				/* dump($result['content']);
				dump(json_decode($result['content']));*/
				echo CHtml::decode($result['content']);
				exit;
			} else {
				echo "Вы не выбрали файл";
				exit;
			}
		}
		$this->render('upload_avatar', array(
			'model' => $model,
		));
	}

	/*function base64_encode_image($imagefile)
	{
		$imgtype = array('jpg', 'gif', 'png');
		$filename = file_exists($imagefile) ? htmlentities($imagefile) : die('Image file name does not exist');
		$filetype = pathinfo($filename, PATHINFO_EXTENSION);
		if (in_array($filetype, $imgtype)) {
			$imgbinary = fread(fopen($filename, "r"), filesize($filename));
		} else {
			die ('Invalid image type, jpg, gif, and png is only allowed');
		}
		return 'data:image/' . $filetype . ';base64,' . base64_encode($imgbinary);
	}*/

	public function actionChanges()
	{
		$model = new Userinfo;
		if (isset($_POST['Userinfo'])) {
			$url = Yii::app()->params['connectUrl'] . '?c=user&task=changes';
			$result = post_content($url, $_POST['Userinfo']);
			// dump($result['content']);
			//dump(json_decode($result['content']));
			echo CHtml::decode($result['content']);
			exit;
		}
		$this->render('userinfo', array(
			'model' => $model,
		));
	}

}