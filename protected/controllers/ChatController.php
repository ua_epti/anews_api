<?php
class ChatController extends Controller
{

    public function actions()
    {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha'=>array(
                'class'=>'CCaptchaAction',
                'backColor'=>0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page'=>array(
                'class'=>'CViewAction',
            ),
        );
    }


	public function actionSend($action)
	{
		$modelUser = new LoginForm();
		$model = new Chat();
		if (isset ($_POST['LoginForm'])){
			$url = Yii::app()->params['connectUrl'] . $_POST['action'];
            $result = post_content($url, $_POST['LoginForm']);
			$var_user_id = json_decode($result['content'])->responce->Data->id;
		}
		if (json_decode($result['content'])->responce->Result == 'error'){
			die('Вы не авторизованы');
		}
		if ($var_user_id != NULL){
//            if (isset($_POST['Chat'])){
			$_POST['Chat']['userId'] = $var_user_id;
			$url=Yii::app()->params['connectUrl'].'/?c=chat&task=send_message';
			$result = post_content($url,$_POST['Chat']);
			echo CHtml::decode($result['content']);
			//	dump(json_decode($result['content']));
			//echo CHtml::decode($result['content']);
			die;
//            }
		}
		$this->render('send',array('model'=>$model, 'modelUser'=>$modelUser, 'action' => $action));
	}

    public function actionView()
    {   
        $model = new Chat();
        if (isset($_POST['Chat'])){
            $url=Yii::app()->params['connectUrl'].'/?c=chat&task=messages'; 
            $result = post_content($url,$_POST['Chat']);
            /*dump($result['content']);
            dump(json_decode($result['content']));*/
	        echo CHtml::decode($result['content']);
	        exit;
        }
        $this->render('view',array('model'=>$model));
    }
    
    public function actionNew()
    {   
        $model = new Chat();
        if (isset($_POST['Chat'])){
            $url=Yii::app()->params['connectUrl'].'/?c=chat&task=messages_new'; 
            $result = post_content($url,$_POST['Chat']);
            /*dump($result['content']);
            dump(json_decode($result['content']));*/
	        echo CHtml::decode($result['content']);
	        exit;
        }
        $this->render('new',array('model'=>$model));
    }    
    
    public function actionCountNew()
    {   
        $model = new Chat();
        if (isset($_POST['Chat'])){
            $url=Yii::app()->params['connectUrl'].'/?c=chat&task=count_new'; 
            $result = post_content($url,$_POST['Chat']);
           /* dump($result['content']);
            dump(json_decode($result['content']));*/
	        echo CHtml::decode($result['content']);
	        exit;
        }
        $this->render('count_new',array('model'=>$model));
    }   
    
    public function actionRead()
    {   
        $model = new Chat();
        if (isset($_POST['Chat'])){
            $url=Yii::app()->params['connectUrl'].'/?c=chat&task=messages_read'; 
            $result = post_content($url,$_POST['Chat']);
           /* dump($result['content']);
            dump(json_decode($result['content']));*/
	        echo CHtml::decode($result['content']);
	        exit;
        }
        $this->render('read',array('model'=>$model));
    }    
    
    
    public function actionError()
    {
        if($error=Yii::app()->errorHandler->error)
        {
            if(Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }


   
}