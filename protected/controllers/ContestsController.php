<?php
class ContestsController extends Controller
{

    public function actions()
    {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha'=>array(
                'class'=>'CCaptchaAction',
                'backColor'=>0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page'=>array(
                'class'=>'CViewAction',
            ),
        );
    }

  
  public function actionError()
  {
        if($error=Yii::app()->errorHandler->error)
        {
            if(Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
  }

    
   public function actionIndex(){    
       
       
        $url=Yii::app()->params['connectUrl'].'/list?code='.Yii::app()->session['code'].'&model=contest_category'; 
        $result = post_content($url,'');
        
        if ($result['http_code']==200)
        {
            $models=json_decode($result['content']);
            $this->render('categotyList',array(
                    'models'=>$models,
            ));
        }
        else
        {
            echo $result['content'];
            exit;
            //$this->render('categotyList',array('error'=>$result['content'],));
        }
   } 
   
    
   public function actionList($id){    
        $url=Yii::app()->params['connectUrl'].'/list?code='.Yii::app()->session['code'].'&model=contests&cont_category='.$id; 
        $_POST['status']=2;
        $_POST['approved']=3;
        $result = post_content($url,$_POST);
        if ($result['http_code']==200)
        {
            $models=json_decode($result['content']);
            $this->render('list',array(
                    'models'=>$models,
            ));
        }
        else
        {
            echo $result['content'];
            exit;
            //$this->render('list',array('error'=>$result['content'],));
        }
       
    }
    
    
    public function actionView($id)
    {   
        $url=Yii::app()->params['connectUrl'].'/view?code='.Yii::app()->session['code'].'&model=contests&id='.$id; 
        $result = post_content($url,'');
        if ($result['http_code']==200)
        {
            $model=json_decode($result['content']);
            $this->render('view',array(
                'model'=>$model,
            ));
        }
        else
        {
           // $this->render('view',array('error'=>$result['content'],));
           echo $result['content'];
           exit;
        }
 
    }
   

}