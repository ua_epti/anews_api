<?php

class CommentController extends Controller
{
	public function actions()
	{
		return array(
			'captcha' => array(
				'class' => 'CCaptchaAction',
				'backColor' => 0xFFFFFF,
			),
			'page' => array(
				'class' => 'CViewAction',
			),
		);
	}

	public function actionIndex()
	{
		$model = new Comment();
		if (isset($_POST['Comment'])) {
			$url = Yii::app()->params['connectUrl'] . '/?c=comment';
			$result = post_content($url, $_POST['Comment']);
			dump($result['content']);
			dump(json_decode($result['content']));
			exit;
		}
		$this->render('index', array('model' => $model));
	}

	public function actionChanges()
	{
		$model = new Comment();
		if (isset($_POST['Comment'])) {
			$url = Yii::app()->params['connectUrl'] . '/?c=comment&task=changes';
			$result = post_content($url, $_POST['Comment']);

			dump($result['content']);
			dump(json_decode($result['content']));
			exit;
		}
		$this->render('changes', array('model' => $model));
	}

	public function actionUpdate()
	{
		$model = new Comment();
		if (isset($_POST['Comment'])) {
			$url = Yii::app()->params['connectUrl'] . '/?c=comment&task=update';
			$result = post_content($url, $_POST['Comment']);

			dump($result['content']);
			dump(json_decode($result['content']));
			exit;
		}
		$this->render('update', array('model' => $model));
	}


	public function actionError()
	{
		if ($error = Yii::app()->errorHandler->error) {
			if (Yii::app()->request->isAjaxRequest) {
				echo $error['message'];
			} else {
				$this->render('error', $error);
			}
		}
	}


}