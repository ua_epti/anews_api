<?php

class PlaylistController extends Controller
{

	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha' => array(
				'class' => 'CCaptchaAction',
				'backColor' => 0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page' => array(
				'class' => 'CViewAction',
			),
		);
	}

	public function actionIndex()
	{
		$model = new Playlist();
		if (isset($_POST['Playlist'])) {
			$url = Yii::app()->params['connectUrl'] . '/?c=playlist';
			$result = post_content($url, $_POST['Playlist']);
			/*dump($result['content']);
			dump(json_decode($result['content']));*/
			echo CHtml::decode($result['content']);
			exit;
		}
		$this->render('index', array('model' => $model));
	}

	public function actionInfo()
	{
		$model = new Playlist();
		if (isset($_POST['Playlist'])) {
			$url = Yii::app()->params['connectUrl'] . '/?c=playlist&task=info';
			$result = post_content($url, $_POST['Playlist']);
			/*dump($result['content']);
			dump(json_decode($result['content']));*/
			echo CHtml::decode($result['content']);
			exit;

		}
		$this->render('info', array('model' => $model));

	}


	public function actionCreate()
	{
		$model = new Playlist();
		if (isset($_POST['Playlist'])) {
			$url = Yii::app()->params['connectUrl'] . '/?c=playlist&task=create';
			$result = post_content($url, $_POST['Playlist']);
			/*dump($result['content']);
			dump(json_decode($result['content']));*/
			echo CHtml::decode($result['content']);
			exit;

		}
		$this->render('create', array('model' => $model));
	}

	public function actionUpdate()
	{
		$model = new Playlist();
		if (isset($_POST['Playlist'])) {
			$url = Yii::app()->params['connectUrl'] . '/?c=playlist&task=update';
			$result = post_content($url, $_POST['Playlist']);
			/*dump($result['content']);
			dump(json_decode($result['content']));*/
			echo CHtml::decode($result['content']);
			exit;

		}
		$this->render('update', array('model' => $model));
	}


	public function actionDelete()
	{
		$model = new Playlist();
		if (isset($_POST['Playlist'])) {
			$url = Yii::app()->params['connectUrl'] . '/?c=playlist&task=remove';
			$result = post_content($url, $_POST['Playlist']);
			/*dump($result['content']);
			dump(json_decode($result['content']));*/
			echo CHtml::decode($result['content']);
			exit;
		}
		$this->render('delete', array('model' => $model));
	}


	public function actionShare()
	{
		$model = new Playlist();
		if (isset($_POST['Playlist'])) {
			$url = Yii::app()->params['connectUrl'] . '/?c=playlist&task=share';
			$result = post_content($url, $_POST['Playlist']);
			/*dump($result['content']);
			dump(json_decode($result['content']));*/
			echo CHtml::decode($result['content']);
			exit;
		}
		$this->render('share', array('model' => $model));
	}


	public function actionUnshare()
	{
		$model = new Playlist();
		if (isset($_POST['Playlist'])) {
			$url = Yii::app()->params['connectUrl'] . '/?c=playlist&task=unshare';
			$result = post_content($url, $_POST['Playlist']);
			/*dump($result['content']);
			dump(json_decode($result['content']));*/
			echo CHtml::decode($result['content']);
			exit;

		}
		$this->render('unshare', array('model' => $model));
	}

	public function actionUsersPlaylists()
	{
		$url = Yii::app()->params['connectUrl'] . '/?c=playlist&task=users_playlists';
		$result = post_content($url, '');
		/*dump($result['content']);
		dump(json_decode($result['content']));*/
		echo CHtml::decode($result['content']);
		exit;
	}

	public function actionDownload()
	{
		if (isset($_POST['Playlist'])) {

			$url = Yii::app()->params['connectUrl'] . '/?c=playlist&task=download';
			$result = post_content($url, $_POST['Playlist']);

			if (!isset(json_decode($result['content'])->responce->Data[0]->url)){
				echo ('Для данного пользователя записей не найдено!');
			}
			$str_title = '';
			$i=0;
			$count = count(json_decode($result['content'])->responce->Data);
			foreach (json_decode($result['content'])->responce->Data as $value) {
				if ($i< ($count)-1)
				{
				if ($str_title != $value->title){
					echo '<br>';
					echo $value->title;
					echo '<br>';
				}
					$str_title = $value->title;
				$exp_val = $value->url;
				echo CHtml::link($exp_val, array('playlist/get_file', 'fileName' => $exp_val,'path' => array_pop(json_decode($result['content'])->responce->Data)));
				echo '</br>';
				}
				$i++;
			}
			exit;
		} else {
			$model = new Playlist();
			$this->render('download', array('model' => $model));
		}
	}


	public function actionError()
	{
		if ($error = Yii::app()->errorHandler->error) {
			if (Yii::app()->request->isAjaxRequest) {
				echo $error['message'];
			} else {
				$this->render('error', $error);
			}
		}
	}

	public function actionGet_file($fileName, $path)
	{
		if ($fileName !== NULL) {
			$currentFile = $path.$fileName;
			if ($currentFile) {
				header('Content-Description: File Transfer');
				header("Content-Type: audio/mpeg, audio/x-mpeg, audio/x-mpeg-3, audio/mpeg3");
				header("Content-Transfer-Encoding: binary");
				header("Accept-Ranges: bytes");
				header("Content-Length: " . filesize($currentFile));
				header('Content-Disposition: attachment; filename="' . $fileName . '"');
				header('X-Pad: avoid browser bug');
				header('Cache-Control: no-cache');
				readfile($currentFile);
			}
		} else {
			$this->redirect('playlist/download');
		}
	}

}