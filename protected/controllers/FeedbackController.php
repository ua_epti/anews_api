<?php

class FeedbackController extends Controller
{
     public function actions(){
        return array(
        );
    }
    
    public function actionIndex()
    {
            $urlGetFeedback=Yii::app()->params['connectUrl'].'/list?code='.Yii::app()->session['code'].'&model=feedback'; 
            $result = post_content($urlGetFeedback,'');  
            
            if ($result['http_code']!=200){
                $this->render('index',array('error'=>$result['content']));
            }else{
                $models=json_decode($result['content']);
                $this->render('index',array('models'=>$models));
            }
            
    }
    
    public function actionAnswer($id)
    {
            $urlGetUser=Yii::app()->params['connectUrl'].'/list?code='.Yii::app()->session['code'].'&model=answerByFeedbackId&feedback_id='.$id; 
            $result = post_content($urlGetUser,'');  
            
            if ($result['http_code']!=200){
                echo ($result['content']);
                
            }else{
                
                $models=json_decode($result['content']);
                $this->render('answer',array('models'=>$models));
                
            }
            
            
            
            
            
            
            
            
    }
    
    public function actionCreate(){
        
        $model=new FeedbackForm;
        $urlFeedbackCategory=Yii::app()->params['connectUrl'].'/list?model=feedback_category'; 
        $resultFeedbackCategory = post_content($urlFeedbackCategory,'');
        
        $feedbackCategory = json_decode($resultFeedbackCategory['content']);
        
        
        if(isset($_POST['FeedbackForm']))
        {
           if (!Yii::app()->user->isGuest){
                $urlGetUser=Yii::app()->params['connectUrl'].'/view?code='.Yii::app()->session['code'].'&model=users'; 
                $result = post_content($urlGetUser,'');  
                $user=json_decode($result['content']);
                $_POST['FeedbackForm']['first_name']=$user->first_name;
                $_POST['FeedbackForm']['last_name']=$user->last_name;
                $_POST['FeedbackForm']['email']=$user->email;
            }
            
            $url=Yii::app()->params['connectUrl'].'/feedback'; 
            $result = post_content($url,$_POST['FeedbackForm']);
            
            Yii::app()->user->setFlash('feedback','Благодарим Вас за запрос. Мы постараемся ответить вам как можно скорее.');    
            
        }

        $this->render('create',array(
            'model'=>$model,
            'feedbackCategory'=>$feedbackCategory,
            
        ));
    }
    
    
    
    
    
  
    public function actionError()
    {
        if($error=Yii::app()->errorHandler->error)
        {
            if(Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    
    
  
    
  /*  public function actionList(){    
    
        $url=Yii::app()->params['connectUrl'].'/list?code='.Yii::app()->session['code'].'&model=trenks'; 
        $result = post_content($url,'');
        if ($result['http_code']==200)
        {
            $trenks=json_decode($result['content']);
            $this->render('list',array(
                    'trenks'=>$trenks,
            ));
        }
        else
        {
            $this->render('list',array('error'=>$result['content'],));
        }
    }
    public function actionView($id)
        {
            $url=Yii::app()->params['connectUrl'].'/view?code='.Yii::app()->session['code'].'&model=trenks&id='.$id; 
            $result = post_content($url,'');
            if ($result['http_code']==200)
            {
                $trenk=json_decode($result['content']);
                $this->render('view',array(
                        'trenk'=>$trenk,
                ));
            }
            else
            {
                $this->render('view',array('error'=>$result['content'],));
            }
 
    }
    public function actionCreate(){
        $model=new TrenkForm;
        if(isset($_POST['TrenkForm']))
        {     
            $_POST['TrenkForm']['date_create'] = $_POST['TrenkForm']['date_update'] = date('Y-m-d h:n:s', time());
            
            $urlUser=Yii::app()->params['connectUrl'].'/view?code='.Yii::app()->session['code'].'&model=users'; 
            $returnUser = post_content($urlUser,'');
        
            if ($returnUser['http_code']==200)
            {
                $user=json_decode($returnUser['content']);
                
            }else {
                echo $result['content'];
                exit;
            }
            
            $_POST['TrenkForm']['id_user']=$user->id_user;
            
            $url=Yii::app()->params['connectUrl'].'/create?code='.Yii::app()->session['code'].'&model=trenks'; 
            $result = post_content($url,$_POST['TrenkForm']);

            $this->redirect(array('list'));
        }

        $this->render('create',array(
            'model'=>$model,
        ));
    }
    
    
    public function actionUpdate($id){
         $url=Yii::app()->params['connectUrl'].'/view?code='.Yii::app()->session['code'].'&model=trenks&id='.$id; 
         $result = post_content($url,'');
         
         if ($result['http_code']!=200){
            $this->render('update',array('error'=>$result['content'],));  
         }else{
            $trenk=json_decode($result['content']);    
             
            $model=new TrenkForm;
            $model->id=$trenk->id;
            $model->id_user=$trenk->id_user;
            $model->title=$trenk->title;
            $model->date_create=$trenk->date_create;
            $model->date_update=$trenk->date_update;
          
            if(isset($_POST['TrenkForm']))
            {                            
                $_POST['TrenkForm']['date_create'] = $_POST['TrenkForm']['date_update'] = date('Y-m-d h:n:s', time());                             
                $url=Yii::app()->params['connectUrl'].'/update?code='.Yii::app()->session['code'].'&model=trenks&id='.$id; 
                $result = post_content($url,$_POST['TrenkForm']);
                $this->redirect(array('view','id'=>$model->id));
            }
            $this->render('update',array(
                'model'=>$model,
            )); 
         }
    }
    
    public function actionDelete($id){
         $url=Yii::app()->params['connectUrl'].'/delete?code='.Yii::app()->session['code'].'&model=trenks&id='.$id; 
         $result = post_content($url,'');
         if ($result['http_code']!=200){
            print $result['content'];
         }else{
            $this->redirect(array('list'));
         }
    }    */
    
    
    

}