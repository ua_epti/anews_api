<?php

class SiteController extends Controller
{

	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	
	public function actionIndex()
	{
		$this->render('index');

	}
  
	public function actionError()
	{
	    if($error=Yii::app()->errorHandler->error)
	    {
	    	if(Yii::app()->request->isAjaxRequest)
	    		echo $error['message'];
	    	else
	        	$this->render('error', $error);
	    }
	}
	public function actionAuthenticate($action)
	{
		$model = new LoginForm();
		$this->render('login', array(
			'model'=>$model,
			'action'=>$action
		));
	}

	public function actionLogin()
	{
		if (isset($_POST)){
			$var_user = $_POST['LoginForm']['username'];
			$var_password = $_POST['LoginForm']['password'];
			$arr_user_data = array('email'=>$var_user,'password'=>$var_password);
			$url = Yii::app()->params['connectUrl'] . $_POST['action'];
			$result = post_content($url, $arr_user_data);
			if ((json_decode($result['content'])->responce->Result == 'success')) {
				//dump(json_decode($result['content']));
				echo CHtml::decode($result['content']);die;
			}
            else if (json_decode($result['content'])->responce->Message == "Ничего не найдено!"){
                //dump(json_decode($result['content']));
	            echo CHtml::decode($result['content']);die;
            }
			else {
                echo 'Вы не авторизованы';
				echo '</br>';
				echo CHtml::link('Вернуться','index');
			}
		}
	}

    public function actionBuyAudioguides(){
        $modelUser = new LoginForm();
        $modelAudioguides = new Audioguides();
        if ((isset($_POST['LoginForm'])) && (isset($_POST['Audioguides']))){
            $arr_user_data = array('email'=>$_POST['LoginForm']['username'],'password'=>$_POST['LoginForm']['password'],'audioguides'=>$_POST['Audioguides']['audioguides']);
            $url = Yii::app()->params['connectUrl'] . '/?c=audioguides&task=buy';
            $result = post_content($url, $arr_user_data);
          //  dump(json_decode($result['content']));die;
	        echo CHtml::decode($result['content']);die;
        }
        $this->render('buyAudioguides', array('modelUser'=>$modelUser,'modelAudioguides'=>$modelAudioguides));
    }



	public function actionAppleBuy(){
		$modelUser = new LoginForm();
		$modelAudioguides = new Audioguides();
		if (isset($_POST['Category'])){
			$arr_user_data = array('inappID'=>$_POST['Audioguides']['inappID'],'audioguides'=>$_POST['Audioguides']['audioguides'],'user_id'=>$_POST['Audioguides']['user_id']);
			$url = Yii::app()->params['connectUrl'] . '/?c=site&task=AppleBuy';
			$result = post_content($url, $_POST['Category']);
			echo CHtml::decode($result['content']);die;
			//dump(json_decode($result['content']));die;
		}
		$this->render('appleBuy', array('modelUser'=>$modelUser,'modelAudioguides'=>$modelAudioguides));
	}

}