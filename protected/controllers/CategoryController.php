<?php

class CategoryController extends Controller
{

	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha' => array(
				'class' => 'CCaptchaAction',
				'backColor' => 0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page' => array(
				'class' => 'CViewAction',
			),
		);
	}

	public function actionIndex()
	{
		$model = new Category();

		if (isset($_POST['Category'])) {
			$url = Yii::app()->params['connectUrl'] . '/?c=category';
			$result = post_content($url, $_POST['Category']);
			echo $result['content'];
//			dump(json_decode($result['content']));
			exit;

		}
		$this->render('index', array('model' => $model));
	}

	public function actionChanges()
	{
		$model = new Category();
		if (isset($_POST['Category'])) {
			$url = Yii::app()->params['connectUrl'] . '/?c=category&task=changes';
			$result = post_content($url, $_POST['Category']);
			dump($result['content']);
			dump(json_decode($result['content']));
			exit;

		}
		$this->render('changes', array('model' => $model));
	}

	public function actionDownloadItem()
	{
		$model = new Category();
		if (isset($_POST['Category'])) {
			$url = Yii::app()->params['connectUrl'] . '/?c=category&task=download_item';
			$result = post_content($url, $_POST['Category']);
			dump($result['content']);
			dump(json_decode($result['content']));
			exit;
		}
		$this->render('download_item', array('model' => $model));
	}

	public function actionUpdate()
	{
		$model = new Category();
		if (isset($_POST['Category'])) {
			$url = Yii::app()->params['connectUrl'] . '/?c=category&task=update';
			$result = post_content($url, $_POST['Category']);
			dump($result['content']);
			dump(json_decode($result['content']));
			exit;
		}
		$this->render('update', array('model' => $model));
	}

	public function actionPurchased()
	{
		$model = new Category();
		if (isset($_POST['Category'])) {
			$url = Yii::app()->params['connectUrl'] . '/?c=category&task=purchased';
			$result = post_content($url, $_POST['Category']);
			dump($result['content']);
			dump(json_decode($result['content']));
			exit;

		}
		$this->render('purchased', array('model' => $model));
	}

	public function actionError()
	{
		if ($error = Yii::app()->errorHandler->error) {
			if (Yii::app()->request->isAjaxRequest) {
				echo $error['message'];
			} else {
				$this->render('error', $error);
			}
		}
	}

	public function actionLastUpdate()
	{
		if (isset($_POST['Category'])) {
			$url = Yii::app()->params['connectUrl'] . '/?c=category&task=lastUpdateCategory';
			$result = post_content($url, $_POST['Category']);
			dump(json_decode($result['content']));
			exit;
		} else {
			$model = new Category();
			$this->render('lastupdate', array('model' => $model));
		}
	}
}