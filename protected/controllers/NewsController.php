<?
class NewsController extends Controller
{
	public function actions()
	{
		return array(
			'captcha' => array(
				'class' => 'CCaptchaAction',
				'backColor' => 0xFFFFFF,
			),
			'page' => array(
				'class' => 'CViewAction',
			),
		);
	}

	public function actionIndex()
	{
		$model = new News();
		if (isset($_POST['News'])) {
			$url = Yii::app()->params['connectUrl'] . '/?c=news&task=all';
			$result = post_content($url, $_POST['News']);
			echo $result['content'];
			exit;
		}
		$this->render('index', array('model' => $model));
	}

	public function actionMynews()
	{
		$model = new News();
		if (isset($_POST['News'])) {
			$url = Yii::app()->params['connectUrl'] . '/?c=news&task=Byuser';
			$result = post_content($url, $_POST['News']);
			echo $result['content'];
			exit;
		}
		$this->render('mynews', array('model' => $model));
	}

	public function actionUploadImage()
	{
		$model = new News();
		if (isset($_POST['News'])) {
			$file_name_with_full_path = '';
			if (isset($_FILES)) {
				$name = $_FILES['News']['name']['image'];
				$ext = end(explode(".", $name));
				$file_name_with_full_path = realpath($_FILES['News']['tmp_name']['image']);
				$new_file_name = $file_name_with_full_path . '.' . $ext;
				rename($file_name_with_full_path, $new_file_name);
			}
			$target_url = Yii::app()->params['connectUrl'] . '?c=user&task=upImage';
			$post = array('News[image]' => '@' . $new_file_name, 'newsID' => $_POST['News']['newsID']);
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $target_url);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
			$result = curl_exec($ch);
			curl_close($ch);
			echo $result['content'];
			die;
		}
		$this->render('upload_image', array(
			'model' => $model,
		));
	}

	public function actionUploadVideo()
	{
		$model = new News();
		if (isset($_POST['News'])) {



			/*$file_name_with_full_path = '';
			if (isset($_FILES)) {
				$name = $_FILES['News']['name']['video'];
				$ext = end(explode(".", $name));
				$file_name_with_full_path = realpath($_FILES['News']['tmp_name']['video']);
				$new_file_name = $file_name_with_full_path . '.' . $ext;
				rename($file_name_with_full_path, $new_file_name);
			}
			$target_url = Yii::app()->params['connectUrl'] . '?c=user&task=upVideo';
			$post = array('News[video]' => '@' . $new_file_name, 'newsID' => $_POST['News']['newsID']);
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $target_url);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
			$result = curl_exec($ch);
			curl_close($ch);*/
//			echo $result['content'];
			die;
		}
		$this->render('upload_video', array(
			'model' => $model,
		));
	}
}