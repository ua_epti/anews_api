<?php

class TrenkController extends Controller
{

    public function actions()
    {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha'=>array(
                'class'=>'CCaptchaAction',
                'backColor'=>0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page'=>array(
                'class'=>'CViewAction',
            ),
        );
    }

    
    public function actionIndex()
    {
        $this->render('index');
    }
  
    public function actionError()
    {
        if($error=Yii::app()->errorHandler->error)
        {
            if(Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    
    
  
    
    public function actionList(){    
    
        $url=Yii::app()->params['connectUrl'].'/list?code='.Yii::app()->session['code'].'&model=trenks'; 
        $result = post_content($url,'');
        if ($result['http_code']==200)
        {
            $trenks=json_decode($result['content']);
            $this->render('list',array(
                    'trenks'=>$trenks,
            ));
        }
        else
        {
            echo $result['content'];
            exit;
            //$this->render('list',array('error'=>$result['content'],));
        }
    }
    public function actionView($id)
        {
            $url=Yii::app()->params['connectUrl'].'/view?code='.Yii::app()->session['code'].'&model=trenks&id='.$id; 
            $result = post_content($url,'');
            if ($result['http_code']==200)
            {
                $trenk=json_decode($result['content']);
                $this->render('view',array(
                        'trenk'=>$trenk,
                ));
            }
            else
            {
                echo $result['content'];
                exit;
                //$this->render('view',array('error'=>$result['content'],));
            }
 
    }
    public function actionCreate(){
        $model=new TrenkForm;
        if(isset($_POST['TrenkForm']))
        {     
            $_POST['TrenkForm']['date_create'] = $_POST['TrenkForm']['date_update'] = date('Y-m-d h:n:s', time());
            
            $urlUser=Yii::app()->params['connectUrl'].'/view?code='.Yii::app()->session['code'].'&model=users'; 
            $returnUser = post_content($urlUser,'');
        
            if ($returnUser['http_code']==200)
            {
                $user=json_decode($returnUser['content']);
                
            }else {
                echo $result['content'];
                exit;
            }
            
            $_POST['TrenkForm']['id_user']=$user->id_user;
            
            $url=Yii::app()->params['connectUrl'].'/create?code='.Yii::app()->session['code'].'&model=trenks'; 
            $result = post_content($url,$_POST['TrenkForm']);

            $this->redirect(array('list'));
        }

        $this->render('create',array(
            'model'=>$model,
        ));
    }
    
    
    public function actionUpdate($id){
         $url=Yii::app()->params['connectUrl'].'/view?code='.Yii::app()->session['code'].'&model=trenks&id='.$id; 
         $result = post_content($url,'');
         
         if ($result['http_code']!=200){
             echo $result['content'];
            exit;
            //$this->render('update',array('error'=>$result['content'],));  
         }else{
            $trenk=json_decode($result['content']);    
             
            $model=new TrenkForm;
            $model->id=$trenk->id;
            $model->id_user=$trenk->id_user;
            $model->title=$trenk->title;
            $model->date_create=$trenk->date_create;
            $model->date_update=$trenk->date_update;
          
            if(isset($_POST['TrenkForm']))
            {                            
                $_POST['TrenkForm']['date_create'] = $_POST['TrenkForm']['date_update'] = date('Y-m-d h:n:s', time());                             
                $url=Yii::app()->params['connectUrl'].'/update?code='.Yii::app()->session['code'].'&model=trenks&id='.$id; 
                $result = post_content($url,$_POST['TrenkForm']);
                $this->redirect(array('view','id'=>$model->id));
            }
            $this->render('update',array(
                'model'=>$model,
            )); 
         }
    }
    
    public function actionDelete($id){
         $url=Yii::app()->params['connectUrl'].'/delete?code='.Yii::app()->session['code'].'&model=trenks&id='.$id; 
         $result = post_content($url,'');
         if ($result['http_code']!=200){
            print $result['content'];
         }else{
            $this->redirect(array('list'));
         }
    }
    
    
    

}